package com.youtube.tutorials.multithreading.synchronization;

//https://www.youtube.com/watch?v=kCmczFg3Kio&feature=youtu.be

public class Counter1 implements Runnable{

	private int i;
	private static final int endValue = 1000;
	private Object obj;
	
	public Counter1(){
		this.i = 0;
		this.obj = new Object();
	}
	
	public void run(){
		for(int i = 0; i < endValue;i++){
			iterate();
		}
	}
	
	public void iterate(){
		synchronized(this){
			if(this.i < endValue){
				System.out.println(Thread.currentThread().getName() + ": " + this.i++);
			}
		}
	}
	
	public static void main(String[] args){
		Runnable r = new Counter1();
		Thread t1 = new Thread(r, "thread 1");
		Thread t2 = new Thread(r, "thread 2");
		Thread t3 = new Thread(r, "thread 3");
		t1.start();
		t2.start();
		t3.start();
	}
	
}
