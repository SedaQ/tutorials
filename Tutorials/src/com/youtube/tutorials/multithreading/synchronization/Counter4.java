package com.youtube.tutorials.multithreading.synchronization;

public class Counter4 {

	private volatile int i;  //keyword volatile is used for field synchronization
	private static final int endValue = 1000;
	
	public Counter4(){
		this.i = 0;
	}
	
	public void run(){
		for(int i = 0; i < endValue;i++){
			iterate();
		}
	}
	
	public void iterate(){
		if(this.i < endValue){
			System.out.println(Thread.currentThread().getName() + ": " + this.i++);
		}
		
	}
	
	public static void main(String[] args){
		Runnable r = new Counter2();
		Thread t1 = new Thread(r, "thread 1");
		Thread t2 = new Thread(r, "thread 2");
		Thread t3 = new Thread(r, "thread 3");
		t1.start();
		t2.start();
		t3.start();
	}

}
