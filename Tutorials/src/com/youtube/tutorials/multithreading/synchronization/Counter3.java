package com.youtube.tutorials.multithreading.synchronization;

public class Counter3 implements Runnable{
	
	private static int i = 0;
	private static final int endValue = 10000;
	
	public Counter3(){}
	
	public void run(){
		for(int i=0;i<endValue;i++){
			iterate();
			// iterate2();      for static method synchronization
		}
	}
	
	public static void iterate(){
		synchronized(Counter3.class){
			if(i < endValue){
				System.out.println(Thread.currentThread().getName() + ": " + i++);
			}
		}
	}
	
	static synchronized void iterate2(){
		if(i < endValue){
			System.out.println(Thread.currentThread().getName() + ": " + i++);
		}
	}
	
	public static void main(String[] args){
		Thread t1 = new Thread(new Counter3(), "thread 1");
		Thread t3 = new Thread(new Counter3(), "thread 2");
		Thread t2 = new Thread(new Counter3(), "thread 3");
		t1.start();
		t2.start();
		t3.start();
	}

}
