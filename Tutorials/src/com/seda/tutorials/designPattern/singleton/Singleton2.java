package com.seda.tutorials.designPattern.singleton;

public class Singleton2 {

	private static final Object singleton2 = new Object();
	
	private Singleton2(){}
	
	public static Object getInstance(){
		return singleton2;
	}
	
	public static void main(String[] args){
		
	}
}
