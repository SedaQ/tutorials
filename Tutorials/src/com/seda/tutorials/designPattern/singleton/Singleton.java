package com.seda.tutorials.designPattern.singleton;

public class Singleton {

	private static Singleton singleton = null;
	
	private Singleton(){}
	
	public static Singleton getInstance(){
		if(singleton == null){
			synchronized(Singleton.class){
				if(singleton==null){
					singleton = new Singleton();
				}
			}
		}
		return singleton;
	}
	
	public static void printText(){
		System.out.println("Vypis text");
	}

	public static void main(String[] args){
		Singleton singleton = Singleton.getInstance();
		Singleton singleton1 = Singleton.getInstance();
		singleton.printText();
		singleton1.printText();
		Singleton s = new Singleton();
		System.out.println("Jsou instance stejne?: " + (singleton == singleton1));
		System.out.println("Jsou instance stejne?: " + (s == singleton1));
		
	}
	
}
